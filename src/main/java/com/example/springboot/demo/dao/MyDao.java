package com.example.springboot.demo.dao;

import com.example.springboot.demo.entity.MyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MyDao extends JpaRepository<MyEntity,Integer> {
}
