package com.example.springboot.demo.controller;

import com.example.springboot.demo.entity.MyEntity;
import com.example.springboot.demo.service.MyService;
import com.example.springboot.demo.service.OtherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Create by yster@foxmail.com 2019/2/1 0001 15:59
 */
@Controller
public class MyController {
    @Autowired
    private MyService myService;


    @Autowired
    private OtherService otherService;

    @RequestMapping("/demo")
    @ResponseBody
    public String demo() {
        myService.service();
        otherService.findOne(1);
        return "123456";
    }

    @RequestMapping("/")
    @ResponseBody
    public List<MyEntity> findAll() {
        return myService.findAll();
    }

}
