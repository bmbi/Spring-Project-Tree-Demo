package com.example.springboot.demo.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Create by yster@foxmail.com 2019/2/4 0004 21:08
 */
@Entity
@Table(name = "my_entity")
@Data
public class MyEntity {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;

    public MyEntity() {
    }

    public MyEntity(String s) {
        this.name = s;
    }
}
