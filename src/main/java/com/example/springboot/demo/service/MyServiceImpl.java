package com.example.springboot.demo.service;

import com.example.springboot.demo.dao.MyDao;
import com.example.springboot.demo.entity.MyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Create by yster@foxmail.com 2019/2/2 0002 18:20
 */
@Service
public class MyServiceImpl implements MyService {

    @Autowired
    private MyDao myDao;

    @Override
    public void service(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("调用Service方法");
    }

    @Override
    public void save(MyEntity myEntity){
        myDao.save(myEntity);
    }

    @Override
    public Optional<MyEntity> find(Integer id){
        return myDao.findById(id);
    }

    @Override
    public List<MyEntity> findAll() {
        if (myDao.count()<5) {
            save(new MyEntity("小明"));
        }
        find(0);
        return myDao.findAll();
    }

}
