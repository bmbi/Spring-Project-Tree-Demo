package com.example.springboot.demo.service;

import org.springframework.stereotype.Service;

/**
 * Create by yster@foxmail.com 2019/2/7 0007 22:38
 */
@Service
public class OtherServiceImpl implements OtherService {

    @Override
    public int findOne(int id){
        return 1;
    }

}
