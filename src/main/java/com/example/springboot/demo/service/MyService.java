package com.example.springboot.demo.service;

import com.example.springboot.demo.entity.MyEntity;

import java.util.List;
import java.util.Optional;

/**
 * Create by yster@foxmail.com 2019/2/3 0003 14:17
 */
public interface MyService {
    void service();

    void save(MyEntity myEntity);

    Optional<MyEntity> find(Integer id);

    List<MyEntity> findAll();
}
